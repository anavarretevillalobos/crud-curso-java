/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.crud.factory;

import com.mycompany.crud.dao.impl.PersonaDaoImpl;

/**
 *
 * @author anavarretev
 */
public class DaoFactory {
 
    public static Object getDao(String daoName) {
    
        Object dao = null;
        
        switch (daoName) {
            case "PersonaDao":
                dao = new PersonaDaoImpl();
                break;
            default:
                System.err.println("El dao solicitado no existe.");
        }
        
        return dao;
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
