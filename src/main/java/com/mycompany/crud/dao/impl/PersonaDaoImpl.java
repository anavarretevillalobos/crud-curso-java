/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.crud.dao.impl;

import com.mycompany.crud.conexion.Conexion;
import com.mycompany.crud.dao.PersonaDao;
import com.mycompany.crud.entity.Persona;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author anavarretev
 */
public class PersonaDaoImpl implements PersonaDao {

    @Override
    public Persona create(Persona obj) {
        
        try ( SqlSession session = Conexion.getConexion().openSession();) {
           int id =  session.insert("PersonaNP.crearPersona",obj);
           obj.setId(id);            
        } catch (Exception e) {            
            throw new RuntimeException(e);
        }
        return obj;
        /*
        PreparedStatement pstm= null;
        Connection con= null;        
	Conexion conexion = new Conexion();
        
        try {            
                String sql="INSERT INTO persona values (0,?,?,?)";
            
                con = conexion.conectar();
                pstm = con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
                
                pstm.setString(1, obj.getNombre());
                pstm.setString(2, obj.getCorreo());
                pstm.setInt(3, obj.getEdad());
                pstm.execute();
                
                // ANV: Retorna el Id generado por MySQL
                ResultSet generatedKeys = pstm.getGeneratedKeys();
                if (generatedKeys.next()) {
                         obj.setId(generatedKeys.getInt(1));
                }
                
                // Cerrar todos los accesos.
                pstm.close();
                con.close();
                
        } catch (SQLException e) {
                System.out.println("Error al crear");
                e.printStackTrace();
        }
        
        return obj;
        */
    }
    
    @Override
    public void delete(Persona obj) {
        
        
        /*
        PreparedStatement pstm= null;
        Connection con= null;        
	Conexion conexion = new Conexion();
        
        try {            
                String sql="DELETE FROM persona WHERE id = ?";
            
                con = conexion.conectar();
                pstm = con.prepareStatement(sql);                
                
                //ANV: Este es el valor del where id = ?
                pstm.setInt(1, obj.getId());
                pstm.execute();
                                
                // Cerrar todos los accesos.
                pstm.close();
                con.close();
                
        } catch (SQLException e) {
                System.out.println("Error al eliminar");
                e.printStackTrace();
        }
        */
    }

    @Override
    public void update(Persona obj) {
        
        
        
        
        
        /*
        PreparedStatement pstm= null;
        Connection con= null;        
	Conexion conexion = new Conexion();
        
        try {            
                String sql="UPDATE persona SET nombre = ? , correo = ?, edad = ? WHERE id = ?";
            
                con = conexion.conectar();
                pstm = con.prepareStatement(sql);
                pstm.setString(1, obj.getNombre());
                pstm.setString(2, obj.getCorreo());
                pstm.setInt(3, obj.getEdad());
                pstm.setInt(4, obj.getId());
                pstm.execute();
                                
                // Cerrar todos los accesos.
                pstm.close();
                con.close();
                
        } catch (SQLException e) {
                System.out.println("Error al actualizar");
                e.printStackTrace();
        }
        */
    }

    @Override
    public List<Persona> findAll() {
       
        try ( SqlSession session = Conexion.getConexion().openSession();) {
           List<Persona> list =  session.selectList("PersonaNP.findAll");
           return list;        
        } catch (Exception e) {            
            throw new RuntimeException(e);
        }
        
        
        /*
        Connection con =null;
        PreparedStatement pstm= null;
        Conexion conexion = new Conexion();

        String sql="SELECT id,nombre,correo,edad FROM persona";
        
        //String sql="SELECT id,nombre,correo,edad FROM persona WHERE id = ?";

        List<Persona> listaPersonas = new ArrayList<Persona>();

        try {			
                con = conexion.conectar();
                pstm=con.prepareStatement(sql);    
                //pstm.setInt(1, obj.getId());
                ResultSet rs = pstm.executeQuery();                
                
                while (rs.next()) {
                    Persona persona = new Persona();
                    persona.setId(rs.getInt("id"));
                    persona.setNombre(rs.getString("nombre"));
                    persona.setCorreo(rs.getString("correo"));
                    persona.setEdad(rs.getInt("edad"));
                    listaPersonas.add(persona);
                }
                
                pstm.close();
                rs.close();
                con.close();
                
        } catch (SQLException e) {
                System.out.println("Error al listar");
                e.printStackTrace();
        }
        return listaPersonas;
        */
    }

    @Override
    public Persona findById(Persona obj) {
        
        
        try ( SqlSession session = Conexion.getConexion().openSession();) {
           Persona persona =  session.selectOne("PersonaNP.FindById",obj);
           return persona;        
        } catch (Exception e) {            
            throw new RuntimeException(e);
        }
        
        
        
        
        
        
        
        
        
        /*
        Connection con =null;
        PreparedStatement pstm= null;
        Conexion conexion = new Conexion();
        Persona persona = null;
        String sql="SELECT id,nombre,correo,edad FROM persona WHERE id = ?";


        try {			
                con = conexion.conectar();
                pstm=con.prepareStatement(sql);    
                pstm.setInt(1, obj.getId());
                ResultSet rs = pstm.executeQuery();                
                
                while (rs.next()) {
                    persona = new Persona();
                    persona.setId(rs.getInt("id"));
                    persona.setNombre(rs.getString("nombre"));
                    persona.setCorreo(rs.getString("correo"));
                    persona.setEdad(rs.getInt("edad"));
                  
                }
                
                pstm.close();
                rs.close();
                con.close();
                
        } catch (SQLException e) {
                System.out.println("Error al buscar");
                e.printStackTrace();
        }
        return persona;
        */
    }

    @Override
    public List<Persona> FindByDate(Persona persona) {
        
        try ( SqlSession session = Conexion.getConexion().openSession();) {
           List<Persona> list =  session.selectList("PersonaNP.FindByDate",persona);
           return list;        
        } catch (Exception e) {            
            throw new RuntimeException(e);
        }
        
        
        
        
        
        
        
        
        /*
        Connection con = null;
        PreparedStatement pstm= null;
        Conexion conexion = new Conexion();
        List<Persona> listaPersonas = new ArrayList<Persona>();
        
        
        String sql="SELECT id,nombre,correo,edad, fecha_nacimiento FROM persona"
                + " WHERE fecha_nacimiento BETWEEN ? AND ? ";      

        try {			
                con = conexion.conectar();
                pstm=con.prepareStatement(sql);
                java.sql.Date sDate1 = new java.sql.Date(persona.getFechaInicio().getTime());
                pstm.setDate(1, sDate1);
                java.sql.Date sDate2 = new java.sql.Date(persona.getFechaTermino().getTime());
                pstm.setDate(2, sDate2);
                ResultSet rs = pstm.executeQuery();                
                
                while (rs.next()) {
                    Persona personaDate = new Persona();
                    personaDate.setId(rs.getInt("id"));
                    personaDate.setNombre(rs.getString("nombre"));
                    personaDate.setCorreo(rs.getString("correo"));
                    personaDate.setEdad(rs.getInt("edad"));
                    personaDate.setFechaNacimiento(rs.getDate("fecha_nacimiento"));                    
                    listaPersonas.add(personaDate);
                }
        } catch (SQLException e) {
                System.out.println("Error: Clase ClienteDaoImple, método obtener");
                e.printStackTrace();
        }
        return listaPersonas;
        */
    }        
}
    