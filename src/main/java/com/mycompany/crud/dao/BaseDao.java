/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.crud.dao;

import java.util.List;

/**
 *
 * @author anavarretev
 */
public interface BaseDao<T> {
   
    public T create(T obj);
    
    public void delete(T obj);
    
    public void update(T obj);
    
    public List<T> findAll();
    
    public T findById(T obj);
}
