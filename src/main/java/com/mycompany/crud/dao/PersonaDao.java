/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.crud.dao;

import com.mycompany.crud.entity.Persona;
import java.util.List;

/**
 *
 * @author anavarretev
 */
public interface PersonaDao extends BaseDao<Persona> {
    
    public List<Persona> FindByDate(Persona persona);
    
}
