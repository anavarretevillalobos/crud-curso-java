/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.crud.init;

import com.mycompany.crud.dao.PersonaDao;
import com.mycompany.crud.dao.impl.PersonaDaoImpl;
import com.mycompany.crud.entity.Persona;
import com.mycompany.crud.factory.DaoFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author anavarretev
 */
public class Star {
    
    
    public static void main(String[] args) {
     
        /*
        * ANV: Crear Persona
        */
        /*
        Persona personaC = new Persona();
        personaC.setNombre("Pablo");
        personaC.setCorreo("pablo@gmail.com");
        personaC.setEdad(25);
        
        PersonaDao daoCrear = new PersonaDaoImpl();
        
        Persona respuestaPersona = daoCrear.create(personaC);
        
        System.out.println(respuestaPersona);
        */
        /*====================================*/
        /*
        * ANV:  Eliminar Persona
        */
        /*
        Persona personaD = new Persona();
        personaD.setId(1);
        
        PersonaDao daoEliminar = new PersonaDaoImpl();
        daoEliminar.delete(personaD);
       
        
        System.out.println("Persona Eliminada");
        */
        /*====================================*/
        
        
        /*
        * ANV:  Actualizar Persona
        */
        /*
        Persona personaA = new Persona();
        personaA.setId(1);
        personaA.setNombre("Nicolas");
        personaA.setCorreo("pablito.pablo@gmail.com");
        personaA.setEdad(30);
        
        
        PersonaDao daoActualizar = new PersonaDaoImpl();
        daoActualizar.update(personaA);
       
        
        System.out.println("Persona Actualizada");
        */
        /*====================================*/
         
        /*
        * ANV:  Listar las persona
        */
        /* 
        PersonaDao daoListar = (PersonaDao) DaoFactory.getDao("PersonaDao");
        List<Persona> listaPersonas = daoListar.findAll();
        
        for (Persona p : listaPersonas) {
            System.out.println(p);
        }
         */
        /*==========================*/
        //buscar por id
        
        Persona personaB = new Persona();
        personaB.setId(1);
        
        PersonaDao daoBuscar = (PersonaDao) DaoFactory.getDao("PersonaDao");
        Persona personaBuscar = daoBuscar.findById(personaB);
        
                
        System.out.println("Persona buscada por id "+ personaBuscar);
        
/*============================================================================*/
        /*
        * ANV: buscar por fechas
        */
        String fechaInico = "20/05/2020";
        String fechaFin = "21/05/2020";
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        
        try {            
            Persona personaDate = new Persona();
            personaDate.setFechaInicio(formato.parse(fechaInico));
            personaDate.setFechaTermino(formato.parse(fechaFin));
            
            PersonaDao daoBuscarFechas = (PersonaDao) DaoFactory.getDao("PersonaDao");
            List<Persona> listaPersonasPorFecha = daoBuscarFechas.FindByDate(personaDate);       
            
            listaPersonasPorFecha.forEach(p -> System.out.println("Persona Por Fecha : " + p ));
                        
        } 
        catch (ParseException ex) 
        {
            System.out.println(ex);
        }
        
        
        
    }
}
