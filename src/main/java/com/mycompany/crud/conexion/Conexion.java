/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.crud.conexion;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

/**
 *
 * @author anavarretev
 */
public class Conexion {
    
     private static final String RESOURCE = "com/mycompany/crud/xml/config/mybatis-config.xml";
    
    public static SqlSessionFactory getConexion(){
        try{
            InputStream in = Resources.getResourceAsStream(RESOURCE);
            return new SqlSessionFactoryBuilder().build(in);
        } catch(IOException e){
            System.out.println("¡Error de Conexión!");
        }
        return null;
    }
    
    
    
    /*
    private final String URL = "jdbc:mysql://localhost:3306/"; // Ubicación de la BD.
    private final String BD = "crudpersona"; // Nombre de la BD.
    private final String USER = "root";
    private final String PASSWORD = "";
    
    public Connection conexion = null;

    public Connection conectar()  {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection(URL + BD, USER, PASSWORD);
            if (conexion != null) {
                System.out.println("¡Conexión Exitosa!");
            }else{
                System.out.println("¡Error de Conexión!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            return conexion;
        }
    }
    */
    
}
